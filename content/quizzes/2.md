---
title: "Quizz on basic rules"
date: 2019-03-31T10:07:35+02:00
draft: false
cookieSetting: "q2"
returnTo: "index.html#rules"
batch: "2"
nrOfQurestions: "5"
---

# | Quiz on fighting techniques
## Let's see if the training paid off yet <br><br>

{{< quizz >}}

If you want to further examine some of the quiz puzzles, check out the solutions <a href="../../lessons/solutions2" noreferrer noopener><u>here</u></a>. 
